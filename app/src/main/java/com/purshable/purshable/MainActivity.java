package com.purshable.purshable;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.apache.http.Header;
import org.json.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.widget.ExpandableListView;
//import com.loopj.android.http.*;
import com.loopj.android.http.*;

public class MainActivity extends Activity {
    // more efficient than HashMap for mapping integers to objects
    SparseArray<Group> groups = new SparseArray<Group>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        try {
            createData();
        }catch (Exception e){
            e.printStackTrace();
        }
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
        MyExpandableListAdapter adapter = new MyExpandableListAdapter(this,
                groups);
        listView.setAdapter(adapter);
    }

    public void createData() throws JSONException {
        String[] stores = {"Marianos", "Target", "Jewel Osco", "Whole Foods","Green Grocer"};
        String[] items = {"$0.99 Eggs", "$0.79 Milk", "$0.77 Carrots", "$0.61 Potatoes","$0.55 Tomatoes"};
        getPublicTimeline();

        for (int j = 0; j < 5; j++) {
            Group group = new Group(stores[j]);
            for (int i = 0; i < 5; i++) {
                group.children.add(items[i]);
            }
            groups.append(j, group);
        }
    }

    private static final String BASE_URL = "http://purshableservice.azurewebsites.net/api/Products";

    private static AsyncHttpClient client = new AsyncHttpClient();

   /* public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get("", params, responseHandler);
    }*/

    public void getPublicTimeline() throws JSONException {
        client.get(BASE_URL, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline){
                // Pull out the first event on the public timeline
                try{
                    LinkedHashMap<String,List<String>> groupForm = new LinkedHashMap<String,List<String>>();
                    for(int i = 0; i < timeline.length(); i++){
                        JSONObject firstEvent = (JSONObject)timeline.get(i);
                        String storeName = firstEvent.getString("StoreName");
                        List<String> temp = new ArrayList<String>();
                        if(groupForm.containsKey(storeName)){
                            temp = groupForm.get(storeName);
                        }
                        temp.add("$"+firstEvent.getString("DiscountPrice")+" "+firstEvent.getString("Name"));
                        groupForm.put(storeName,temp);
                    }
                    int j =0;
                    for (Map.Entry<String, List<String>> entry : groupForm.entrySet()) {

                        String key = entry.getKey();
                        List<String> value = entry.getValue();
                        Group group = new Group(key);
                        Log.i("key", "" + key);
                        for (int i = 0; i <value.size() ; i++) {
                            Log.i("Value", "" + value.get(i));
                            group.children.add(value.get(i));
                        }
                        groups.append(j++, group);

                    }


                    JSONObject firstEvent = (JSONObject)timeline.get(0);
                String tweetText = firstEvent.getString("StoreName");
                   Log.i("Testing", "" + tweetText);
                // Do something with the response
                System.out.println(tweetText);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

}
