package com.purshable.purshable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shilpika on 4/11/2015.
 */
public class Group {
    public String string;
    public final List<String> children = new ArrayList<String>();

    public Group(String string) {
        this.string = string;
    }
}
